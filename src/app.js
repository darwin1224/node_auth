const express = require('express') 
const app = express()

app.use(express.json())

app.post('/login', (req, res) => {
    if (req.body.username === 'billy' && req.body.password === 'ganteng') {
        res.send({ token: '123456'})
    } else {
        res.status(401).send({ message: 'Gagal login' })
    }
})

app.listen(3000, () => {
    console.log('Server is running')
})
FROM node:12.9.0-alpine

WORKDIR /var/www/html

COPY ./package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "node", "src/app.js" ]